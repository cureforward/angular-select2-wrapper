(function () {
    'use strict';

    angular.module('cfAngularSelect2', [])
        .provider('cfAngularSelect2Config', cfAngularSelect2Config)
        .directive('dropdown', dropdown)
        .constant('cfAngularSelect2Ver', '16'); // TODO: remove later

    function cfAngularSelect2Config() {
        var wrapURLFunction;

        this.wrapURLFunction = function (func) {
            wrapURLFunction = func;
        };

        function wrapURL(url) {
            return (wrapURLFunction || angular.identity)(url);
        }

        this.$get = function () {
            return {
                wrapUrl: wrapURL
            };
        };
    }

    dropdown.$inject = ['cfAngularSelect2Config'];

    function dropdown(cfAngularSelect2Config) {
        return {
            restrict: 'E',
            template: '<div class="cf-select" data-toggle="tooltip" title="{{hint}}">\
                           <select style="width:100%"></select>\
                           <span class="cf-field-label" ng-if="suffix">{{suffix}}</span>\
                       </div>\
                       <div class="cf-form-value-error cf-has-error" ng-show="hasError">{{errorMsg}}</div>',
            scope: {
                ajaxDataParser: '&',
                asObject: '=',
                beforeSelect: '&',
                createTag: '&',
                disabled: '=',
                displayKey: '@',
                dropdownParent: '=',
                errorMsg: '@',
                form: '=',
                headers: '&', // should return object with headers
                hint: '@',
                minSearchLength: '@?',
                multiple: '=',
                name: '@',
                ngModel: '=',
                onAjaxError: '&', // should return promise object
                placeholder: '@',
                qparams: '=',
                required: '=',
                resetValue: '=',
                src: '=',
                tags: '=',
                followCursor: '=',
                setInitialValue: '=',
                url: '@',
                ajaxFetchDelay: '@',
                valueKey: '@',
                addUserDataField: '@?' /*provide name of costum field that should be added to model + use ajaxDataParser to add data property*/
            },
            link: function (scope, el, attrs) {

                var $select = $(el).find('select'),
                    cfg = {
                        placeholder: scope.placeholder,
                        multiple: scope.multiple,
                        tags: scope.tags,
                        disabled: scope.disabled,
                        tokenSeparators: scope.multiple ? [','] : null,
                        dropdownParent: scope.dropdownParent ? $(el) : $(document.body),
                        escapeMarkup: function (markup) {
                            return markup;
                        }
                    };

                if (!scope.name) {
                    scope.name = 'dropdown';
                }

                if (!_.isUndefined(scope.minSearchLength)) {
                    cfg.minimumInputLength = scope.minSearchLength;
                }
                if (attrs.createTag) {
                    cfg.createTag = function (params) {
                        return scope.createTag({
                            params: params
                        });
                    };
                }

                setInitialValue();

                bindDataSource();

                $select.select2(cfg);

                _.defer(function () {
                    // click ouside
                    $('body').on('touchstart', function (e) {
                        var el = $(e.target);
                        if (el.parents('.select2-container').length === 0) {
                            forceCloseDropdown();
                        }
                    });

                    //init hint
                    if (scope.hint) {
                        $('.cf-select', el).tooltip();
                    }

                    // Init follow cursor
                    if (scope.followCursor) {
                        $(el).addClass('cf-follow-cursor');
                    }

                    // needed for triggering keyboard on mobile devices
                    $(el).on('touchstart click', function () {
                        $('.select2-search__field', el).focus();
                    });

                    $(el).on('focus', function () {
                        $select.select2('open');
                    });

                    updateElDefaultPlaceholder();
                });

                $select.on('select2:select', function (e) {

                    setSelected(e.params.data);
                    setChanged();
                    if (angular.isDefined(scope.form)) {
                        scope.form.$setDirty();
                    }
                });


                $select.on('select2:open', function() {
                    // Set new dropdown position that follow cursor
                    if (scope.followCursor) {
                        var activeTagInput,
                            dropdownLeft,
                            dropdownTop;

                        activeTagInput = $(el).find('ul.select2-selection__rendered .select2-search__field');
                        dropdownLeft = activeTagInput.offset().left - $(el).offset().left;

                        if (dropdownLeft) {
                            $(el).find('.select2-dropdown').css({
                                left: dropdownLeft + 'px',
                            });
                        }

                    }
                });

                $select.on('select2:close', function () {
                    setLoadingClass(false);
                    setChanged();
                    resetSelectPos();
                    if(!scope.asObject || scope.setInitialValue) {
                        setInitialValue();
                    }
                });

                $select.on('select2:unselect', function (e) {
                    remSelected(e.params.data);
                    setChanged();
                    if (angular.isDefined(scope.form)) {
                        scope.form.$setDirty();
                    }
                });

                $(el).find('input.select2-search__field').on('keyup', function () {
                    if (this.value.length < scope.minSearchLength) {
                        setLoadingClass(false);
                    }
                });

                scope.$on('dropdownDataReset', function () {
                    resetSelect();
                });

                scope.$on('dropdownDataReset_' + scope.name, function () {
                    resetSelect();
                });

                scope.$watch('ngModel', setInitialValue);

                scope.$watch('src', function (newData) {
                    updateSrcData(newData);
                });

                scope.$watch('disabled', function (value) {
                    //rewrite predefined value
                    cfg.disabled = value;
                    $select.prop('disabled', value);
                });

                scope.$on('$destroy', function () {
                    forceCloseDropdown();
                });

                //////////////////////////////
                /* functions implementation */
                //////////////////////////////

                function bindDataSource() {
                    if (_.isArray(scope.src)) { // for direct source
                        cfg.data = parseData(scope.src);
                        cfg.minimumResultsForSearch = Infinity;
                    }

                    if (_.isString(scope.url)) { // for url
                        cfg.ajax = {
                            url: cfAngularSelect2Config.wrapUrl(scope.url),
                            dataType: 'json',
                            delay: scope.ajaxFetchDelay || 250,

                            data: function (params) {
                                setLoadingClass(true);
                                return _.extend(params, scope.qparams);
                            },
                            processResults: function (data) {

                                var parsedData = parseData(data);


                                // Added class for dropddown results list
                                // TODO: find solution without setTimeout
                                $(el).find('.select2-results').attr('id', scope.name.toLowerCase() + '-results');
                                //set up uid as attribute for each li element
                                setTimeout(function () {
                                    $(el).find('.select2-results li').each(function (i) {
                                        var _this = $(this);
                                        if (parsedData[i]) {
                                            _this.attr('id', generateIdFormText(parsedData[i].id));
                                            _this.attr('data-text', parsedData[i].text);
                                        }
                                    });
                                }, 100);
                                // End

                                if (data.length === 0) {
                                    setLoadingClass(false);
                                }
                                return {
                                    results: parsedData
                                };
                            },
                            cache: true
                        };
                        if (attrs.headers) {
                            cfg.ajax.beforeSend = function (jqXHR) {
                                var headers = scope.headers();

                                if (_.isEmpty(headers)) return;

                                _.each(headers, function (value, name) {
                                    jqXHR.setRequestHeader(name, value);
                                });
                            }
                        }
                        if (attrs.onAjaxError) {
                            cfg.ajax.transport = function (params, success, failure) {
                                var request = $.ajax(params);
                                request.success(success);
                                request.fail(function (jqXHR, textStatus, errorThrown) {
                                    scope.onAjaxError({
                                        jqXHR: jqXHR,
                                        textStatus: textStatus,
                                        errorThrown: errorThrown
                                    }).then(function () {
                                        var request2 = $.ajax(params);
                                        request2.success(success);
                                        request2.fail(failure);
                                    }, failure);
                                });
                                return request;
                            }
                        }
                    }
                }

                function resetSelect() {
                    $select.select2('destroy');
                    scope.ngModel = scope.resetValue ? scope.resetValue : null;
                    $select.html('');
                    $select.select2(cfg);
                }

                function resetSelectPos() {
                    $select.select2(cfg);
                }

                function getNewModel(id, text, data) {
                    var m = null;
                    if (scope.displayKey && scope.valueKey) {
                        m = {};
                        m[scope.valueKey] = id || null;
                        m[scope.displayKey] = text || null;
                        if (data) {
                            m[scope.addUserDataField || 'data'] = data;
                        }
                    } else {
                        m = id || text;
                    }
                    return m;
                }

                function updateSrcData(src) {
                    if (_.isString(scope.url)) return;

                    src = src || [];
                    $select.html('');
                    cfg.data = parseData(src);
                    $select.select2(cfg);
                    if (scope.asObject && scope.ngModel !== null) {
                        $select.val(scope.ngModel[scope.valueKey]).trigger('change');
                    } else {
                        $select.val(scope.ngModel).trigger('change');
                    }
                    updateElDefaultPlaceholder();
                }

                function parseData(data) {
                    var nData = scope.ajaxDataParser({
                        resp: data,
                        baseParser: parseDataBase
                    });
                    if (!_.isArray(nData)) {
                        nData = parseDataBase(data);
                    }
                    return nData;
                }

                function parseDataBase(data) {
                    return _(data).map(function (value) {
                        return {
                            id: scope.valueKey ? value[scope.valueKey] : JSON.stringify(value),
                            text: value[scope.displayKey]
                        };
                    });
                }

                function forceCloseDropdown() {
                    $select.select2('close');
                    $select.parent().tooltip('hide');
                }

                function remSelected(item) {
                    scope.$apply(function () {
                        if (scope.multiple) {
                            scope.ngModel = _(scope.ngModel).filter(function (value) {
                                return item.id !== value[scope.valueKey];
                            });
                        } else {
                            scope.ngModel = null;
                        }
                    });
                }

                function setSelected(item) {
                    var res = scope.beforeSelect({
                        item: item,
                        allItems: $select.data('select2').data()
                    });
                    var newModel;
                    if (_.isArray(res)) {
                        $select.val(res).trigger('change');
                    }
                    scope.$emit('dropdownValueChanged');
                    scope.$apply(function () {
                        if (scope.multiple) {
                            scope.ngModel = [];
                            var items = $select.data('select2').data();
                            _(items).each(function (item) {
                                var m = getNewModel(item.id, item.text, item.data || null);
                                scope.ngModel.push(m);
                            });
                        } else if (scope.asObject) {
                            newModel = getNewModel(item.id, item.text, item.data || null);
                            if (_.isObject(scope.ngModel) && _.size(scope.ngModel) > 2)
                                _.extend(scope.ngModel, newModel);
                            else
                                scope.ngModel = newModel;
                        } else {
                            scope.ngModel = item.id;
                        }
                    });
                }

                function setChanged() {
                    $(el).addClass('cf-changed');
                    updateErrorImmediately();
                }

                function generateIdFormText(value) {
                    if (!value) return;
                    var result = value.length && value.replace(/[\s,]+/g, '-').trim();
                    result = (scope.name + '-' + result).toLowerCase();
                    return result;
                }

                function setInitialValue() {
                    var value, option, text;
                    if (_.isNull(scope.ngModel)) {
                        $select.val(value).trigger('change');
                        return;
                    }
                    if (_.isString(scope.ngModel) && scope.ngModel.trim()) {
                        value = scope.ngModel.trim();
                        text = scope.ngModel.trim();
                    } else if (_.isObject(scope.ngModel)) {
                        value = scope.valueKey ? scope.ngModel[scope.valueKey] : JSON.stringify(scope.ngModel);
                        text = scope.ngModel[scope.displayKey] || '';
                    }
                    if (_.isUndefined(value)) {
                        return;
                    }
                    if (_.isString(scope.url)) {
                        option = $('<option>', {
                            id: generateIdFormText(value),
                            value: value,
                            text: text
                        });
                        $select.html(option);
                    }
                    $select.val(value).trigger('change');
                }

                function updateErrorImmediately() {
                    if (scope.$$phase) {
                        scope.$apply(updateErrorMsgVisibility);
                    }
                }

                function updateErrorMsgVisibility() {
                    scope.hasError = isEmpty() && scope.required && $(el).hasClass('cf-changed');
                    $(el).find('.cf-select').toggleClass('cf-error', !!scope.hasError);
                }

                function isEmpty() {
                    return scope.ngModel === null || typeof scope.ngModel === 'undefined';
                }

                function setLoadingClass(isLoading) {
                    $(el).find('.cf-select').toggleClass('cf-data-loading', isLoading);
                }

                // Fix for Select render title
                function updateElDefaultPlaceholder() {
                    var placeholderEl = el.find('.select2-selection.select2-selection--single .select2-selection__rendered');

                    if (placeholderEl.find('.select2-selection__placeholder').length) {
                        placeholderEl.attr('title', scope.placeholder);
                    }
                }
            }
        };
    }

})();