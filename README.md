# Angular wrapper for select2

For instaling run in console
``` $ bower install angular-select2-wrapper --save```

## Attributes
* ajaxDataParser: '&',
* asObject: '=',
* beforeSelect: '&',
* createTag: '&',
* disabled: '=',
* displayKey: '@',
* dropdownParent: '=',
* errorMsg: '@',
* form: '=',
* headers: '&', // should return object with headers
* hint: '@',
* minSearchLength: '@?',
* multiple: '=',
* name: '@',
* ngModel: '=',
* onAjaxError: '&', // should return promise object
* placeholder: '@',
* qparams: '=',
* required: '=',
* resetValue: '=',
* src: '=',
* tags: '=',
* url: '@',
* ajaxFetchDelay: '@', //add ajax data fetcher delay, to prevent failing requests
* valueKey: '@',
* addUserDataField: '@?' /*provide name of costum field that should be added to model + use ajaxDataParser to add data property*/